import spacy
from qa_dict import QA

nlp = spacy.load("ru_core_news_lg")  # Подключение токенизатора,тэггера, парсера и NER

docs_precomputed = [nlp(x) for x in QA.keys()]  # Предобработка документа "вопрос:ответ"

"""
    Функция для получения наибольшей косинусной схожести 
    посредством выбора максимума из всех схожестей
"""


def get_answer(input: str) -> str:
    max_similarity = 0
    for i in range(len(docs_precomputed)):
        doc_input = nlp(input)

        i_similarity = doc_input.similarity(
            docs_precomputed[i]
        )  # Cравнение схожести на i-ом шаге
        if i_similarity > max_similarity:
            print(i, i_similarity)
            max_similarity = i_similarity
            answer = docs_precomputed[i]
    if max_similarity < 0.5:
        return "Пожалуйста, переформулируйте вопрос"
    else:
        return QA[str(answer)]
